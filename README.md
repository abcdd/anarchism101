# Anarchism 101
## Introduction into anarchism
This is presentation for the workshop on introduction into anarchism. Feel free to contribute and leave some feedback on how it can be changed. If you wanna do workshop yourself - feel free! 

### Reveal JS
Presenstation was done with help of RevealJS - javascript library for presentation development. Presentation is stored in index.html. Open the file in your browset to get started.

For notes on developing presentation in Reveal JS please go to official git repository of the project - https://github.com/hakimel/reveal.js/
